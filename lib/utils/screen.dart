import 'package:flutter/material.dart';

class ScreenSize {
  ScreenSize._();
  static double width(BuildContext context) {
    return MediaQuery.of(context).size.width;
  }

  static double height(BuildContext context) {
    return MediaQuery.of(context).size.height;
  }

  static double getWidth(BuildContext context, double percent) {
    return width(context) * percent;
  }

  static double getHeight(BuildContext context, double percent) {
    return height(context) * percent;
  }
}

Widget sizedBoxWidth(double width) {
  return SizedBox(width: width);
}

Widget sizedBoxHeight(double height) {
  return SizedBox(height: height);
}

Widget dividerView({@required Color? color}) {
  return Divider(thickness: 0.5, color: color);
}
