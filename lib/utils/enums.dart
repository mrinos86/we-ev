enum MapIcons {
  comedy,
  community,
  countryEvents,
  culturalVisualArts,
  exposShows,
  filmsMedia,
  foodDrink,
  music,
  scienceTech,
  sports,
  map,
}

enum ViewEventFrom {
  explore,
  saved,
  myEvents,
}
