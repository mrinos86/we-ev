import 'package:flutter/material.dart';
import 'package:we_ev/utils/assets.dart';

// ignore: must_be_immutable
class AppLogo extends StatelessWidget {
  bool isColor;
  double? imageHeight;
  double? imageWidth;

  AppLogo({super.key, this.isColor = false, this.imageHeight, this.imageWidth});

  @override
  Widget build(BuildContext context) {
    return Hero(
      tag: "appLogo",
      child: Image(
        height: imageHeight ?? 96,
        width: imageWidth ?? 175,
        image: AssetImage(
          isColor ? AppAssets.LOGO_COLOR : AppAssets.LOGO_WHITE,
        ),
        // fit: BoxFit.fitWidth,
      ),
    );
  }
}
