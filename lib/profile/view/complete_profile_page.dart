import 'package:flutter/material.dart';
import 'package:we_ev/app/app.dart';
import 'package:we_ev/auth/auth.dart';
import 'package:we_ev/profile/profile.dart';
import 'package:we_ev/profile/widgets/track_location.dart';
import 'package:we_ev/utils/utils.dart';

class CompleteProfilePage extends StatefulWidget {
  const CompleteProfilePage({super.key});

  @override
  State<CompleteProfilePage> createState() => _CompleteProfilePageState();
}

class _CompleteProfilePageState extends State<CompleteProfilePage> {
  late PageController _pageController;
  int _selectedPageIndex = 2;

  @override
  void initState() {
    _pageController = PageController(initialPage: _selectedPageIndex);
    // getCategoryDetails();
    debugPrint("Complete pro");
    super.initState();
  }

  // Future<void> getCategoryDetails() async {
  //   WidgetsBinding.instance.addPostFrameCallback((_) async {
  //     AppStateServiceProvider.instance.showLoader;
  //     await ProfileRepository.instance.getCategoryData();
  //     AppStateServiceProvider.instance.hideLoader;
  //   });
  // }

  @override
  void dispose() {
    _pageController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async => false,
      child: GestureDetector(
        onTap: () => FocusScope.of(context).unfocus(),
        child: Scaffold(
          appBar: AppBar(
            // title: const Text("Complete Profile"),
            automaticallyImplyLeading: false,
            title: _selectedPageIndex == 1
                ? Text(
                    "Add Vehicle",
                    style: size16_M_medium(),
                  )
                : AppLogo(
                    isColor: true,
                    imageHeight: 44,
                    imageWidth: 74,
                  ),
            leading: Builder(builder: (context) {
              if (_selectedPageIndex > 0) {
                return IconButton(
                  onPressed: () {
                    _pageController.previousPage(
                      duration: const Duration(milliseconds: 300),
                      curve: Curves.easeInOut,
                    );
                  },
                  icon: const Icon(Icons.arrow_back_ios),
                );
              }
              return const SizedBox.shrink();
            }),

            actions: [
              if (_selectedPageIndex == 0)
                Padding(
                  padding: const EdgeInsets.all(15),
                  child: InkWell(
                    onTap: () {
                      AppStateServiceProvider.instance.currentUser = null;
                      AppStateServiceProvider.instance.setEmailVerified = false;
                    },
                    customBorder: const CircleBorder(),
                    child: Text("Logout", style: size16_M_regular()),
                  ),
                ),
            ],
          ),
          body: Builder(
            builder: (context) {
              // if (isClient) {
              //   return const ClientBasicDetails();
              // } else {
              return Padding(
                padding: const EdgeInsets.all(15.0),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    sizedBoxHeight(5),
                    Row(
                      children: [
                        for (int i = 0; i < 4; i++)
                          SizedBox(
                            width: MediaQuery.of(context).size.width / 4.5,
                            child: AnimatedContainer(
                              duration: const Duration(milliseconds: 300),
                              margin: const EdgeInsets.symmetric(horizontal: 2),
                              height: 5,
                              decoration: ShapeDecoration(
                                shape: const StadiumBorder(),
                                color: _selectedPageIndex >= i
                                    ? primaryColor
                                    : Colors.grey,
                              ),
                            ),
                          ),
                      ],
                    ),
                    Expanded(
                      child: PageView(
                        controller: _pageController,
                        onPageChanged: (index) {
                          setState(() {
                            _selectedPageIndex = index;
                          });
                        },
                        physics: const NeverScrollableScrollPhysics(),
                        children: [
                          AboutMe(
                            pageController: _pageController,
                          ),
                          AddVehicle(pageController: _pageController),
                          Notifications(pageController: _pageController),
                          TrackLocation(pageController: _pageController),
                        ],
                      ),
                    ),
                  ],
                ),
              );
              // }
            },
          ),
        ),
      ),
    );
  }
}
