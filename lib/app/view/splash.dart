import 'dart:async';
import 'package:flutter/material.dart';
import 'package:we_ev/app/app.dart';
import 'package:we_ev/app/provider/app_state.dart';
import 'package:we_ev/auth/provider/provider.dart';
import 'package:we_ev/utils/colors.dart';
import 'package:we_ev/utils/text_style.dart';
import 'package:we_ev/utils/utils.dart';

class SplashPage extends StatefulWidget {
  const SplashPage({Key? key}) : super(key: key);

  @override
  State<SplashPage> createState() => _SplashPageState();
}

class _SplashPageState extends State<SplashPage>
    with SingleTickerProviderStateMixin {
  late Animation<double> _animation;
  late AnimationController _animationController;
  late AppStateServiceProvider _appStateServiceProvider;

  @override
  void initState() {
    _animationController = AnimationController(
      vsync: this,
      duration: const Duration(milliseconds: 500),
    );
    _animation = Tween(begin: 0.0, end: 1.0).animate(
      CurvedAnimation(
        parent: _animationController,
        curve: Curves.easeInOut,
      ),
    );
    _appStateServiceProvider = AppStateServiceProvider.instance;
    WidgetsBinding.instance.addPostFrameCallback((_) => startUpCheck());
    super.initState();
  }

  void startUpCheck() async {
    _animationController.forward();
    await Future.delayed(const Duration(seconds: 2));
    await AuthServiceProvider.instance.startUpCheck();
    _appStateServiceProvider.isInitialized = true;
  }

  @override
  void dispose() {
    _animationController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: primaryColor,
      body: Center(
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 32),
          child: SlideTransition(
            position: _animation.drive(Tween<Offset>(
                begin: const Offset(0, 1), end: const Offset(0, 0))),
            child: FadeTransition(
              opacity: _animation,
              child: Align(
                alignment: Alignment.center,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    AppLogo(),
                    sizedBoxHeight(50),
                    Text(
                      "Save Money | Save Time \nEV charging and trips made easy…",
                      textAlign: TextAlign.center,
                      style: size16_M_light(textColor: whiteColor),
                    ),
                  ],
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
}
