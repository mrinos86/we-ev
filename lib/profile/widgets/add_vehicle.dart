import 'dart:io';

import 'package:dotted_border/dotted_border.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:we_ev/common/common.dart';
import 'package:we_ev/common/provider/image_picker.dart';
import 'package:we_ev/utils/utils.dart';

class AddVehicle extends StatefulWidget {
  final PageController pageController;
  const AddVehicle({
    Key? key,
    required this.pageController,
  }) : super(key: key);

  @override
  State<AddVehicle> createState() => _AddVehicleState();
}

class _AddVehicleState extends State<AddVehicle> {
  late TextEditingController _vehicleMakeController;
  late TextEditingController _vehicleModelController;
  late TextEditingController _yearController;
  late TextEditingController _trimController;
  late TextEditingController _kilowattsUsedController;
  late TextEditingController _batteryController;

  String? pickedImages;

  bool isImageError = false;
  bool isVehicleMakeError = false;
  bool isVehicleModelError = false;
  bool isYearError = false;
  bool isTrimError = false;

  String errorMessage = "";

  @override
  void initState() {
    _vehicleMakeController = TextEditingController();
    _vehicleModelController = TextEditingController();
    _yearController = TextEditingController();
    _trimController = TextEditingController();
    _kilowattsUsedController = TextEditingController();
    _batteryController = TextEditingController();

    super.initState();
  }

  @override
  void dispose() {
    _vehicleMakeController.dispose();
    _vehicleModelController.dispose();
    _yearController.dispose();
    _trimController.dispose();
    _kilowattsUsedController.dispose();
    _batteryController.dispose();

    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return CustomScrollView(
      slivers: [
        SliverFillRemaining(
          hasScrollBody: false,
          fillOverscroll: true,
          child: Container(
            width: double.infinity,
            padding: const EdgeInsets.symmetric(horizontal: 3),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                sizedBoxHeight(25),
                Text(
                  "Make EV life easy, WeEV will show and\nselective right chargers for you based\non your vehicle profile.",
                  textAlign: TextAlign.center,
                  style: size16_M_regular(),
                ),
                sizedBoxHeight(40),
                GestureDetector(
                  onTap: () async {
                    setState(() {
                      isImageError = false;
                    });
                    if (FocusScope.of(context).hasFocus) {
                      FocusScope.of(context).unfocus();
                      await Future.delayed(const Duration(milliseconds: 400));
                    }
                    // ignore: use_build_context_synchronously
                    await _pickImagess(context);
                  },
                  child: DottedBorder(
                    radius: const Radius.circular(10),
                    strokeWidth: 1.5,
                    dashPattern: const [4, 4],
                    borderType: BorderType.RRect,
                    color: isImageError ? errorColor : primaryColor,
                    child: Container(
                      color: whiteColor,
                      height: 100,
                      width: 100,
                      child: Stack(
                        children: [
                          pickedImages != null
                              ? ClipRRect(
                                  borderRadius: BorderRadius.circular(10),
                                  child: Image.file(
                                    File(pickedImages!),
                                    height: 100,
                                    width: 100,
                                    fit: BoxFit.cover,
                                  ),
                                )
                              : Container(),
                          Center(
                            child: Icon(
                              Icons.file_upload_outlined,
                              size: 35,
                              color: isImageError ? Colors.red : primaryColor,
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
                sizedBoxHeight(25),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      'Vehicle Make',
                      style: size14_M_regular(),
                    ),
                    sizedBoxHeight(5),
                    CustomTextField(
                      textController: _vehicleMakeController,
                      isError: isVehicleMakeError,
                      keybordType: TextInputType.name,
                      textCapitalization: TextCapitalization.sentences,
                      onTapped: () {
                        setState(() {
                          isVehicleMakeError = false;
                        });
                      },
                      hintText: "",
                      labelText: "Enter your vehicle make name here",
                    ),
                  ],
                ),
                sizedBoxHeight(15),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      'Vehicle Model',
                      style: size14_M_regular(),
                    ),
                    sizedBoxHeight(5),
                    CustomTextField(
                      textController: _vehicleModelController,
                      isError: isVehicleModelError,
                      keybordType: TextInputType.name,
                      textCapitalization: TextCapitalization.sentences,
                      onTapped: () {
                        setState(() {
                          isVehicleModelError = false;
                        });
                      },
                      hintText: "",
                      labelText: "Enter your vehicle model here",
                    ),
                  ],
                ),
                sizedBoxHeight(15),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      'Year',
                      style: size14_M_regular(),
                    ),
                    sizedBoxHeight(5),
                    CustomTextField(
                      textController: _yearController,
                      isError: isYearError,
                      keybordType: TextInputType.number,
                      inputFormat: <TextInputFormatter>[
                        FilteringTextInputFormatter.digitsOnly
                      ],
                      onTapped: () {
                        setState(() {
                          isYearError = false;
                        });
                      },
                      hintText: "",
                      labelText: "Enter your vehicle year here",
                    ),
                  ],
                ),
                sizedBoxHeight(15),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      'Trim',
                      style: size14_M_regular(),
                    ),
                    sizedBoxHeight(5),
                    CustomTextField(
                      textController: _trimController,
                      isError: isTrimError,
                      keybordType: TextInputType.name,
                      textCapitalization: TextCapitalization.sentences,
                      onTapped: () {
                        setState(() {
                          isTrimError = false;
                        });
                      },
                      hintText: "",
                      labelText: "Enter your trim here",
                    ),
                  ],
                ),
                sizedBoxHeight(15),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      'Kilowotts used per km(charge rate)(optional)',
                      style: size14_M_regular(),
                    ),
                    sizedBoxHeight(5),
                    CustomTextField(
                      textController: _kilowattsUsedController,
                      isError: false,
                      keybordType: TextInputType.name,
                      onTapped: () {},
                      hintText: "",
                      labelText: "Enter Kilowotts used per km here",
                    ),
                  ],
                ),
                sizedBoxHeight(15),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      'Battery Degradation (Optional)',
                      style: size14_M_regular(),
                    ),
                    sizedBoxHeight(5),
                    CustomTextField(
                      textController: _batteryController,
                      isError: false,
                      keybordType: TextInputType.name,
                      onTapped: () {},
                      hintText: "",
                      labelText: "Enter Battery Degradation here",
                    ),
                  ],
                ),
                sizedBoxHeight(25),
                const Spacer(),
                Align(
                  alignment: Alignment.center,
                  child: ElevatedButton(
                    style: ElevatedButton.styleFrom(
                      minimumSize: Size(ScreenSize.getWidth(context, 0.6), 48),
                    ),
                    onPressed: onPressedContinueButton,
                    child: const Text("Continue"),
                  ),
                ),
                sizedBoxHeight(ScreenSize.getHeight(context, 0.05)),
              ],
            ),
          ),
        ),
      ],
    );
  }

  Future<void> _pickImagess(BuildContext context) async {
    String? imagePath = await ImageUtil().getImage(
      context: context,
    );
    if (imagePath != null && imagePath.isNotEmpty) {
      setState(() {
        pickedImages = imagePath;
      });
    }
  }

  bool validationCheck() {
    bool validationError = false;
    if (_trimController.text.isEmpty) {
      setState(() {
        isTrimError = true;
        errorMessage = "Please enter trim";
        validationError = true;
      });
    }
    if (_yearController.text.isEmpty) {
      setState(() {
        isYearError = true;
        errorMessage = "Please enter vehicle year";
        validationError = true;
      });
    }
    if (_vehicleModelController.text.isEmpty) {
      setState(() {
        isVehicleModelError = true;
        errorMessage = "Please enter vehicle model";
        validationError = true;
      });
    }
    if (_vehicleMakeController.text.isEmpty) {
      setState(() {
        isVehicleMakeError = true;
        errorMessage = "Please enter vehicle make name ";
        validationError = true;
      });
    }
    if (pickedImages == null) {
      setState(() {
        errorMessage = "Please pick vehicle Image";
        isImageError = true;
        validationError = true;
      });
    }

    return validationError;
  }

  onPressedContinueButton() async {
    bool isError = validationCheck();
    if (isError) {
      await showOKDialog(
        context: context,
        labelTitle: "Error !",
        labelContent: errorMessage,
      );
    } else {
      widget.pageController.nextPage(
        duration: const Duration(milliseconds: 300),
        curve: Curves.easeInOut,
      );
    }
  }
}
