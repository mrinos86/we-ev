import 'package:flutter/material.dart';
import 'package:we_ev/utils/utils.dart';

// ignore: must_be_immutable
class LeaderBoardWidget extends StatelessWidget {
  LeaderBoardWidget({
    super.key,
    required this.title,
    required this.icon,
    required this.count,
    this.borderType = BorderType.none,
  });
  String title;
  IconData icon;
  String count;
  BorderType borderType;

  @override
  Widget build(BuildContext context) {
    return Expanded(
      child: Container(
        decoration: BoxDecoration(
          border: Border(
            top: BorderSide(
              color: borderType == BorderType.topRight ||
                      borderType == BorderType.top
                  ? greyColor.withOpacity(0.4)
                  : Colors.transparent,
            ),
            right: BorderSide(
              color: borderType == BorderType.right ||
                      borderType == BorderType.topRight
                  ? greyColor.withOpacity(0.4)
                  : Colors.transparent,
            ),
          ),
        ),
        child: Padding(
          padding: const EdgeInsets.fromLTRB(15, 10, 15, 10),
          child: Column(
            children: [
              sizedBoxHeight(5),
              Container(
                width: 105,
                padding: const EdgeInsets.fromLTRB(12, 10, 12, 10),
                decoration: common_dec(
                    borderRadius: 12, color: primaryColor.withOpacity(0.15)),
                child: Row(
                  children: [
                    Container(
                      height: 40,
                      width: 40,
                      decoration: const BoxDecoration(
                          shape: BoxShape.circle, color: whiteColor),
                      child: Icon(
                        icon,
                        color: primaryColor,
                        size: 24,
                      ),
                    ),
                    sizedBoxWidth(8),
                    Text(
                      count,
                      style: size22_M_semibold(textColor: primaryColor),
                    )
                  ],
                ),
              ),
              Text(title,
                  textAlign: TextAlign.center, style: size16_M_regular()),
            ],
          ),
        ),
      ),
    );
  }
}

enum BorderType { topRight, right, top, none }
