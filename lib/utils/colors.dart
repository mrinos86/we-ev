import 'package:flutter/material.dart';

const Color primaryColor = Color(0xFF064622);

const Color backgroundColor = Color(0xFFF7F7F7);
const Color bgColorECF4F1 = Color(0xFFECF4F1);
const Color onSurfaceColor = Color(0xFF121111);
const Color onBackGround = Color(0xFF131313);
const Color textFieldOutlineColor = Color(0xFF2e4052);
const Color disabledBorderGrey = Color(0xFF676D81);
const Color textFieldColor = Color(0xFFEBEBEB);

const Color whiteColor = Colors.white;
const Color blackColor = Colors.black;
const Color greyColor = Colors.grey;
const Color redColor = Color(0xFFEB6150);
const Color darkGreyColor = Color(0xff2e4052);
const Color liteGreyColor = Color(0xffe9ecef);
const Color grey495057 = Color(0xff495057);
const Color yellowFFD045 = Color(0xffFFD045);
const Color yellowEEA014 = Color(0xffEEA014);
const Color brown4D280B = Color(0xff4D280B);

const Color errorColor = Color(0xFFEB6150);
const Color warningColor = Color(0xFFFDCF41);
const Color successColor = Color(0xFF5CFF7E);
