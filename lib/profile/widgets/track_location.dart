import 'dart:io';

import 'package:flutter/material.dart';
import 'package:we_ev/app/app.dart';
import 'package:we_ev/auth/auth.dart';
import 'package:we_ev/utils/utils.dart';

class TrackLocation extends StatefulWidget {
  final PageController pageController;
  const TrackLocation({
    Key? key,
    required this.pageController,
  }) : super(key: key);

  @override
  State<TrackLocation> createState() => _TrackLocationState();
}

class _TrackLocationState extends State<TrackLocation> {
  @override
  Widget build(BuildContext context) {
    return CustomScrollView(
      slivers: [
        SliverFillRemaining(
          hasScrollBody: false,
          fillOverscroll: true,
          child: Container(
            width: double.infinity,
            padding: const EdgeInsets.symmetric(horizontal: 3),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                sizedBoxHeight(25),
                Text(
                  "Track Your Location",
                  style: size18_M_semiBold(textColor: primaryColor),
                ),
                sizedBoxHeight(15),
                Text(
                  "Plan trips without the hassle, We EV\ncalculated travel time, charge time, and the\nbest chargers in the network!",
                  textAlign: TextAlign.center,
                  style: size14_M_regular().copyWith(height: 1.7),
                ),
                sizedBoxHeight(20),
                Text(
                  "Let us track your location to provide you\nwith the best experience.",
                  textAlign: TextAlign.center,
                  style: size14_M_regular().copyWith(height: 1.7),
                ),
                sizedBoxHeight(25),
                ElevatedButton(
                  style: ElevatedButton.styleFrom(
                    minimumSize:
                        Size((MediaQuery.of(context).size.width / 3), 50),
                    maximumSize:
                        Size(MediaQuery.of(context).size.width / 1.6, 50),
                  ),
                  onPressed: () {},
                  child: Row(
                    children: [
                      const Icon(
                        Icons.my_location,
                        size: 22,
                      ),
                      sizedBoxWidth(10),
                      const Text("Use My Current Location"),
                    ],
                  ),
                ),
                const Spacer(),
                ElevatedButton(
                  style: ElevatedButton.styleFrom(
                    minimumSize: Size(ScreenSize.getWidth(context, 0.6), 48),
                  ),
                  onPressed: () {
                    AppStateServiceProvider.instance.currentUser = User();
                    AppStateServiceProvider.instance.setEmailVerified = true;
                    AppStateServiceProvider.instance.setProfileCompleted = true;
                  },
                  child: const Text("Continue"),
                ),
                sizedBoxHeight(ScreenSize.getHeight(context, 0.04)),
              ],
            ),
          ),
        ),
      ],
    );
  }
}
