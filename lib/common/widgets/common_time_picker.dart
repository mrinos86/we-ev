import 'package:flutter/material.dart';
import 'package:we_ev/utils/utils.dart';

// ignore: must_be_immutable
class CommonTimePicker extends StatefulWidget {
  Function onSelectedCallBack;
  String? displayText;
  bool isTimePicked;
  bool? isOnPressedEnabled;
  bool isError;
  CommonTimePicker(
      {super.key,
      required this.onSelectedCallBack,
      this.displayText,
      required this.isTimePicked,
      this.isOnPressedEnabled,
      this.isError = false});

  @override
  State<CommonTimePicker> createState() => _CommonTimePickerState();
}

class _CommonTimePickerState extends State<CommonTimePicker> {
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () async {
        TimeOfDay? pickedS = await showTimePicker(
          initialTime: TimeOfDay.now(),
          context: context,
        );
        widget.onSelectedCallBack(pickedS);
      },
      child: Container(
        padding: const EdgeInsets.symmetric(horizontal: 15, vertical: 12),
        decoration: common_border_dec(
          borderRadius: 8,
          borderColor: widget.isError ? errorColor : greyColor,
        ),
        child: Row(
          children: [
            Expanded(
                child: Text(widget.displayText ?? "Add time",
                    style: size14_M_regular(
                        textColor:
                            widget.isTimePicked ? blackColor : greyColor))),
            Icon(
              Icons.access_time_outlined,
              size: 25,
              color: widget.isError ? errorColor : primaryColor,
            ),
          ],
        ),
      ),
    );
  }
}
