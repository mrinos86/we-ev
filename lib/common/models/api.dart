class NoPayloadResponse {
  String? message;
  bool? result;

  NoPayloadResponse({this.message, this.result});

  NoPayloadResponse.fromJson(Map<String, dynamic> json) {
    message = json['message'];
    result = json['result'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['message'] = message;
    data['result'] = result;
    return data;
  }
}

class BooleanResponse {
  bool? message;
  bool? result;

  BooleanResponse({this.message, this.result});

  BooleanResponse.fromJson(Map<String, dynamic> json) {
    message = json['message'];
    result = json['result'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['message'] = message;
    data['result'] = result;
    return data;
  }
}

class DynamicPayloadResponse {
  DynamicPayloadResponse(
      {this.message, this.payload, this.result, this.errorCode});

  DynamicPayloadResponse.fromJson(dynamic json) {
    message = json['message'];
    payload = json['payload'] != null ? payload : null;
    result = json['result'];
    errorCode = json['errorCode'];
  }

  String? message;
  dynamic payload;
  bool? result;
  int? errorCode;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['message'] = message;
    if (payload != null) {
      map['payload'] = payload;
    }
    map['result'] = result;
    map['errorCode'] = errorCode;
    return map;
  }
}

class ApiResponse<T> {
  final bool result;
  final String message;
  final T? payload;

  ApiResponse({
    this.result = true,
    this.message = '',
    this.payload,
  });
}
