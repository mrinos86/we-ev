class CategoryResModel {
  CategoryResModel({this.categoryList, this.message, this.result});

  List<UserCategory>? categoryList;
  String? message;
  bool? result;

  CategoryResModel.fromJson(dynamic json) {
    if (json['payload'] != null) {
      categoryList = [];
      json['payload'].forEach((v) {
        categoryList?.add(UserCategory.fromJson(v));
      });
    }
    message = json['message'];
    result = json['result'];
  }

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    if (categoryList != null) {
      map['payload'] = categoryList?.map((v) => v.toJson()).toList();
    }

    map['message'] = message;
    map['result'] = result;
    return map;
  }
}

class UserCategory {
  int? id;
  String? name;
  UserCategory({this.id, this.name});

  factory UserCategory.fromJson(Map<String, dynamic> json) =>
      UserCategory(id: json["id"], name: json["name"]);

  Map<String, dynamic> toJson() => {"id": id, "name": name};
}
