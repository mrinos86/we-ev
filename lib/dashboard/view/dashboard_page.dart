import 'package:flutter/material.dart';
import 'package:we_ev/dashboard/dashboard.dart';
import 'package:we_ev/utils/utils.dart';

class DashBoardPage extends StatefulWidget {
  const DashBoardPage({super.key});

  @override
  State<DashBoardPage> createState() => _DashBoardPageState();
}

class _DashBoardPageState extends State<DashBoardPage> {
  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async => false,
      child: Scaffold(
        body: Stack(
          children: <Widget>[
            Image(
              image: const AssetImage(AppAssets.HOME_BG_IMAGE),
              width: MediaQuery.of(context).size.width,
              height: MediaQuery.of(context).size.height,
            ),
            Padding(
              padding: const EdgeInsets.only(
                  top: 0, bottom: 16, left: 12, right: 12),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  SafeArea(
                    maintainBottomViewPadding: true,
                    child: CustomAppBar(
                      userName: "John Doe",
                    ),
                  ),
                  sizedBoxHeight(15),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text(
                        "My Vehilces",
                        style: size18_M_bold(textColor: whiteColor),
                      ),
                      Container(
                          padding: const EdgeInsets.fromLTRB(12, 6, 12, 6),
                          decoration: common_border_dec(
                              borderRadius: 16,
                              color: yellowEEA014,
                              borderColor: brown4D280B),
                          child: Row(
                            children: [
                              const Image(
                                image: AssetImage(AppAssets.HOME_BOLTS),
                                width: 17,
                                height: 20,
                              ),
                              sizedBoxWidth(10),
                              Text(
                                "110 Lighting bolts",
                                style: size14_M_medium(textColor: brown4D280B),
                              ),
                            ],
                          ))
                    ],
                  ),
                  sizedBoxHeight(20),
                  Expanded(
                    child: SingleChildScrollView(
                      child: Column(
                        children: [
                          Stack(
                            alignment: Alignment.bottomCenter,
                            children: [
                              ClipRRect(
                                borderRadius: BorderRadius.circular(18),
                                child: Image(
                                  image:
                                      const AssetImage(AppAssets.SAMPLE_IMG1),
                                  width: MediaQuery.of(context).size.width,
                                  height: 350,
                                  fit: BoxFit.fill,
                                ),
                              ),
                              Padding(
                                padding: const EdgeInsets.all(18),
                                child: Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  children: [
                                    Text("AUDI",
                                        style: size21_M_bold(
                                            textColor: whiteColor)),
                                    Column(
                                      children: [
                                        Text("A7E 2022",
                                            style: size16_M_medium(
                                                textColor: whiteColor)),
                                        Text("All road",
                                            style: size16_M_medium(
                                                textColor: whiteColor)),
                                      ],
                                    ),
                                  ],
                                ),
                              )
                            ],
                          ),
                          sizedBoxHeight(20),
                          Container(
                            decoration: common_dec(
                              borderRadius: 12,
                              color: const Color(0xFFE4DED8),
                            ),
                            padding: const EdgeInsets.fromLTRB(15, 12, 15, 16),
                            child: Column(
                              children: [
                                Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  crossAxisAlignment: CrossAxisAlignment.end,
                                  children: [
                                    const Image(
                                      image: AssetImage(
                                          AppAssets.WEATHER_PARTY_CLOUDY),
                                      fit: BoxFit.cover,
                                      height: 40,
                                      width: 40,
                                    ),
                                    sizedBoxWidth(15),
                                    Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: [
                                        Text(
                                          "20 °C",
                                          style: size24_M_bold(),
                                        ),
                                        Text(
                                          "Party Cloudy",
                                          style: size12_M_medium(),
                                        ),
                                      ],
                                    ),
                                  ],
                                ),
                                sizedBoxHeight(12),
                                Container(
                                  padding:
                                      const EdgeInsets.fromLTRB(15, 16, 25, 12),
                                  decoration: common_dec(
                                      borderRadius: 8, color: brown4D280B),
                                  child: Row(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceBetween,
                                    children: [
                                      Row(
                                        children: [
                                          const Icon(
                                            Icons.location_pin,
                                            size: 32,
                                            color: whiteColor,
                                          ),
                                          sizedBoxWidth(12),
                                          Column(
                                            crossAxisAlignment:
                                                CrossAxisAlignment.start,
                                            children: [
                                              Text(
                                                "Melbourne, VIC",
                                                style: size16_M_semibold(
                                                    textColor: whiteColor),
                                              ),
                                              Text(
                                                "23 March 2023 | 8.30 AM",
                                                style: size12_M_regular(
                                                    textColor: whiteColor),
                                              ),
                                            ],
                                          ),
                                        ],
                                      ),
                                      const Icon(
                                        Icons.arrow_forward_ios,
                                        size: 15,
                                        color: whiteColor,
                                      )
                                    ],
                                  ),
                                )
                              ],
                            ),
                          ),
                          sizedBoxHeight(15),
                          Container(
                            padding: const EdgeInsets.only(
                                left: 15, right: 15, top: 10, bottom: 10),
                            decoration: common_dec(
                                borderRadius: 16, boxShadow: containerShadow),
                            child: Column(
                              children: [
                                Row(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    LeaderBoardWidget(
                                      title: "Community Level",
                                      icon: Icons.groups,
                                      count: "10",
                                      borderType: BorderType.right,
                                    ),
                                    LeaderBoardWidget(
                                      title: "Check Ins",
                                      icon: Icons.groups,
                                      count: "10",
                                    ),
                                  ],
                                ),
                                Row(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    LeaderBoardWidget(
                                      title: "Community Rank",
                                      icon: Icons.groups,
                                      count: "10",
                                      borderType: BorderType.topRight,
                                    ),
                                    LeaderBoardWidget(
                                      title: "Comments / Photos",
                                      icon: Icons.groups,
                                      count: "10",
                                      borderType: BorderType.top,
                                    ),
                                  ],
                                ),
                              ],
                            ),
                          ),

                          // Container(
                          //   padding: const EdgeInsets.only(left: 20, right: 20),
                          //   decoration: common_dec(
                          //       borderRadius: 16, boxShadow: containerShadow),
                          //   child: Expanded(
                          //     child: GridView.builder(
                          //         physics: const NeverScrollableScrollPhysics(),
                          //         shrinkWrap: true,
                          //         itemCount: 4,
                          //         gridDelegate:
                          //             const SliverGridDelegateWithFixedCrossAxisCount(
                          //           mainAxisSpacing: 8,
                          //           mainAxisExtent: 100,
                          //           crossAxisSpacing: 10,
                          //           childAspectRatio: 1,
                          //           crossAxisCount: 2,
                          //         ),
                          //         itemBuilder: (_, index) {
                          //           return Container(
                          //             decoration: BoxDecoration(
                          //               border: Border(
                          //                 top: BorderSide(
                          //                   color: index == 0 || index == 1
                          //                       ? Colors.transparent
                          //                       : Colors.grey,
                          //                 ),
                          //                 right: BorderSide(
                          //                   color: index % 2 != 1
                          //                       ? Colors.grey
                          //                       : Colors.transparent,
                          //                 ),
                          //               ),
                          //             ),
                          //             child:
                          //             Padding(
                          //               padding: const EdgeInsets.all(0),
                          //               child: Column(
                          //                 children: [
                          //                   Container(
                          //                     padding: const EdgeInsets.all(10),
                          //                     decoration: common_dec(
                          //                         borderRadius: 12,
                          //                         color: primaryColor
                          //                             .withOpacity(0.15)),
                          //                     child: Row(
                          //                       children: [
                          //                         Container(
                          //                           height: 40,
                          //                           width: 40,
                          //                           decoration:
                          //                               const BoxDecoration(
                          //                                   shape:
                          //                                       BoxShape.circle,
                          //                                   color: whiteColor),
                          //                           child: const Icon(
                          //                             Icons.groups,
                          //                             color: primaryColor,
                          //                             size: 24,
                          //                           ),
                          //                         ),
                          //                         sizedBoxWidth(10),
                          //                         Text(
                          //                           "15",
                          //                           style: size22_M_semibold(
                          //                               textColor:
                          //                                   primaryColor),
                          //                         )
                          //                       ],
                          //                     ),
                          //                   ),
                          //                   Expanded(
                          //                     child: Text("Community Level",
                          //                         textAlign: TextAlign.center,
                          //                         style: size16_M_regular()),
                          //                   ),
                          //                 ],
                          //               ),
                          //             ),
                          //           );
                          //         }),
                          //   ),
                          // ),

                          sizedBoxHeight(125),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
