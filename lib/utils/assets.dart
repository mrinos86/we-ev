// ignore_for_file: constant_identifier_names, non_constant_identifier_names

class AppAssets {
  AppAssets._();

  // Logo
  static const String LOGO_WHITE = 'assets/logo/ic_logo_white.png';
  static const String LOGO_COLOR = 'assets/logo/ic_logo_color.png';
  static const String GOOGLE_LOGO = 'assets/logo/ic_google.png';
  static const String PROFILE_IMAGE = 'assets/images/ph_profile_image.png';

  static const String PROFILE_COMPLETE_EV_CHARGE =
      "assets/images/img_complete_profile_ev_charge.png";

  static const String HOME_BG_IMAGE = "assets/images/bg_img_home.png";
  static const String HOME_BOLTS = "assets/images/ic_home_bolts.png";

  static const String SAMPLE_IMG1 = "assets/sample_images/sample_img_01.png";

  static const String WEATHER_PARTY_CLOUDY =
      "assets/images/ic_weather_party_cloudy.png";
}
