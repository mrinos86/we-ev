import 'package:flutter/material.dart';
import 'package:go_router/go_router.dart';
import 'package:we_ev/router/router.dart';
import 'package:we_ev/utils/utils.dart';

class NavController extends StatefulWidget {
  final Widget? child;
  const NavController({
    Key? key,
    this.child,
  }) : super(key: key);

  @override
  State<NavController> createState() => _NavControllerState();
}

class _NavControllerState extends State<NavController> {
  int onRouteChangeCurrentIndex() {
    const String bottomNavPrefix = '/bottomNav';
    final location = GoRouter.of(context).location;
    if (location
        .startsWith(Pages.dashboard.toPath(pathPrefix: bottomNavPrefix))) {
      return 0;
    } else if (location
        .startsWith(Pages.mapAssist.toPath(pathPrefix: bottomNavPrefix))) {
      return 1;
    } else if (location
        .startsWith(Pages.market.toPath(pathPrefix: bottomNavPrefix))) {
      return 2;
    } else if (location
        .startsWith(Pages.more.toPath(pathPrefix: bottomNavPrefix))) {
      return 3;
    } else {
      return 0;
    }
  }

  @override
  Widget build(BuildContext context) {
    return AnimatedBuilder(
      animation: GoRouter.of(context),
      builder: (context, child) {
        return Scaffold(
          extendBody: true,
          body: widget.child,
          bottomNavigationBar: ClipRRect(
            borderRadius: const BorderRadius.only(
              topRight: Radius.circular(14),
              topLeft: Radius.circular(14),
            ),
            child: Theme(
              data: Theme.of(context).copyWith(canvasColor: primaryColor),
              child: BottomNavigationBar(
                selectedItemColor: whiteColor,
                unselectedItemColor: whiteColor.withOpacity(0.3),
                showSelectedLabels: true,
                showUnselectedLabels: true,
                selectedLabelStyle: size12_M_medium(),
                unselectedLabelStyle: size12_M_regular(),
                currentIndex: onRouteChangeCurrentIndex(),
                items: const [
                  BottomNavigationBarItem(
                    icon: Padding(
                      padding: EdgeInsets.only(bottom: 5, top: 5),
                      child: Icon(Icons.bar_chart_outlined, size: 28),
                    ),
                    label: 'Dashboard',
                  ),
                  BottomNavigationBarItem(
                    icon: Padding(
                      padding: EdgeInsets.only(bottom: 5, top: 5),
                      child: Icon(Icons.explore, size: 28),
                    ),
                    label: 'Map Assist',
                  ),
                  BottomNavigationBarItem(
                    icon: Padding(
                      padding: EdgeInsets.only(bottom: 5, top: 5),
                      child: Icon(Icons.shopping_cart, size: 28),
                    ),
                    label: 'Market',
                  ),
                  BottomNavigationBarItem(
                    icon: Padding(
                      padding: EdgeInsets.only(bottom: 5, top: 5),
                      child: Icon(Icons.more_vert, size: 28),
                    ),
                    label: 'More',
                  ),
                ],
                onTap: (index) {
                  if (index == 0) {
                    Pages.dashboard.push(context);
                  } else if (index == 1) {
                    Pages.mapAssist.push(context);
                  } else if (index == 2) {
                    Pages.market.push(context);
                  } else if (index == 3) {
                    Pages.more.push(context);
                  }
                },
              ),
            ),
          ),
        );
      },
    );
  }
}
