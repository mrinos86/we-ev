import 'dart:async';
import 'dart:developer';

import 'package:flutter/material.dart';
import 'package:we_ev/app/app.dart';
import 'package:we_ev/auth/auth.dart';
import 'package:we_ev/common/common.dart';

Future<void> bootstrap(FutureOr<Widget> Function() builder) async {
  FlutterError.onError = (details) {
    log(details.exceptionAsString(), stackTrace: details.stack);
  };

  // Initialize Required Plugins and Services
  WidgetsFlutterBinding.ensureInitialized();
  await DeviceInfo.initialize();
  await StorageService.initialize();
  AppStateServiceProvider.initialize();
  AuthServiceProvider.initialize();

  // Run the app in a zone to catch all uncaught errors.
  await runZonedGuarded(
    () async => runApp(await builder()),
    (error, stackTrace) => log(error.toString(), stackTrace: stackTrace),
  );
}
