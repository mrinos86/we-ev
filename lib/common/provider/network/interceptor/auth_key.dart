import 'package:dio/dio.dart';
import 'package:we_ev/auth/auth.dart';

class AccessTokenInterceptor extends Interceptor {
  final AuthServiceProvider _authService = AuthServiceProvider.instance;

  @override
  void onRequest(RequestOptions options, RequestInterceptorHandler handler) {
    if (_authService.user != null) {
      options.headers
          .addEntries([MapEntry('x-access-token', '${_authService.token}')]);
    }
    super.onRequest(options, handler);
  }
}
