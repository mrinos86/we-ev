import 'package:flutter/material.dart';
import 'package:go_router/go_router.dart';
import 'package:we_ev/app/app.dart';
import 'package:we_ev/auth/auth.dart';
import 'package:we_ev/bottom_nav_bar/view/nav_controller.dart';
import 'package:we_ev/dashboard/view/dashboard_page.dart';
import 'package:we_ev/map_assist/map_assist_page.dart';
import 'package:we_ev/market/market_page.dart';
import 'package:we_ev/more/more.dart';
import 'package:we_ev/profile/profile.dart';
import 'package:we_ev/router/router.dart';

final GlobalKey<NavigatorState> _rootNavigatorKey = GlobalKey<NavigatorState>();
final GlobalKey<NavigatorState> _shellNavigatorKey =
    GlobalKey<NavigatorState>();

class AppRouter {
  final AppStateServiceProvider _appStateServiceProvider;

  AppRouter(this._appStateServiceProvider);
  final String bottomNavPrefix = '/bottomNav';

  GoRouter get goRouter => _goRouter;

  late final GoRouter _goRouter = GoRouter(
    refreshListenable: _appStateServiceProvider,
    initialLocation: '/',
    navigatorKey: _rootNavigatorKey,
    debugLogDiagnostics: true,
    routes: <RouteBase>[
      GoRoute(
        path: Pages.splash.toPath(),
        name: Pages.splash.toPathName(),
        pageBuilder: (context, state) =>
            MaterialPage(key: state.pageKey, child: const SplashPage()),
      ),
      GoRoute(
          path: Pages.intro.toPath(),
          name: Pages.intro.toPathName(),
          pageBuilder: (context, state) =>
              MaterialPage(key: state.pageKey, child: const IntroPage()),
          routes: [
            GoRoute(
              path: Pages.signUp.toPath(isSubRoute: true),
              name: Pages.signUp.toPathName(),
              pageBuilder: (context, state) => MaterialPage(
                  key: state.pageKey,
                  child: SignUpPage(
                      isClient: state.queryParameters.containsKey('type')
                          ? state.queryParameters['type'] == 'client'
                          : true)),
            ),
            GoRoute(
                path: Pages.signIn.toPath(isSubRoute: true),
                name: Pages.signIn.toPathName(),
                pageBuilder: (context, state) =>
                    MaterialPage(key: state.pageKey, child: const SignInPage()),
                routes: [
                  GoRoute(
                      path: Pages.forgotPassword.toPath(isSubRoute: true),
                      name: Pages.forgotPassword.toPathName(),
                      parentNavigatorKey: _rootNavigatorKey,
                      pageBuilder: (context, state) => MaterialPage(
                          key: state.pageKey,
                          child: const ForgotPasswordPage()),
                      routes: [
                        GoRoute(
                          path: Pages.forgotPasswordSuccess
                              .toPath(isSubRoute: true),
                          name: Pages.forgotPasswordSuccess.toPathName(),
                          pageBuilder: (context, state) {
                            String email = state.extra as String;
                            return MaterialPage(
                                key: state.pageKey,
                                child: ForgotPasswordSuccessPage(email: email));
                          },
                        ),
                      ]),
                ]),
          ]),
      GoRoute(
        path: Pages.emailVerification.toPath(),
        name: Pages.emailVerification.toPathName(),
        pageBuilder: (context, state) => MaterialPage(
            key: state.pageKey, child: const EmailVerificationPage()),
      ),
      GoRoute(
        path: Pages.completeProfile.toPath(),
        name: Pages.completeProfile.toPathName(),
        pageBuilder: (context, state) => MaterialPage(
            key: state.pageKey, child: const CompleteProfilePage()),
      ),
      ShellRoute(
        navigatorKey: _shellNavigatorKey,
        builder: (context, state, child) {
          return NavController(child: child);
        },
        routes: <RouteBase>[
          GoRoute(
            path: Pages.dashboard.toPath(pathPrefix: bottomNavPrefix),
            name: Pages.dashboard.toPathName(),
            pageBuilder: (context, state) => NoTransitionPage(
              key: state.pageKey,
              child: const DashBoardPage(),
            ),
          ),
          GoRoute(
            path: Pages.mapAssist.toPath(pathPrefix: bottomNavPrefix),
            name: Pages.mapAssist.toPathName(),
            pageBuilder: (context, state) => NoTransitionPage(
                key: state.pageKey, child: const MapAssistPage()),
          ),
          GoRoute(
            path: Pages.market.toPath(pathPrefix: bottomNavPrefix),
            name: Pages.market.toPathName(),
            pageBuilder: (context, state) =>
                NoTransitionPage(key: state.pageKey, child: const MarketPage()),
          ),
          GoRoute(
            path: Pages.more.toPath(pathPrefix: bottomNavPrefix),
            name: Pages.more.toPathName(),
            pageBuilder: (context, state) =>
                NoTransitionPage(key: state.pageKey, child: const MorePage()),
          ),
        ],
      ),
      GoRoute(
        path: Pages.notFound.toPath(),
        name: Pages.notFound.toPathName(),
        pageBuilder: (context, state) => MaterialPage(
            key: state.pageKey,
            child: NotFoundPage(error: state.error.toString())),
      ),
    ],
    errorPageBuilder: (context, state) => MaterialPage(
        key: state.pageKey, child: NotFoundPage(error: state.error.toString())),
    redirect: (context, state) async {
      final stateName = _goRouter.routeInformationParser.configuration;
      final String splashPath =
          stateName.namedLocation(Pages.splash.toPathName());
      final String introPath =
          stateName.namedLocation(Pages.intro.toPathName());
      final String emailVerifyPath =
          stateName.namedLocation(Pages.emailVerification.toPathName());
      final String completeProfilePath =
          stateName.namedLocation(Pages.completeProfile.toPathName());

      final bool isInitialized = _appStateServiceProvider.isInitialized;
      final bool isSignedIn = _appStateServiceProvider.isLoggedIn;
      final bool isEmailVerified = _appStateServiceProvider.isEmailVerified;
      final bool isProfileCompleted =
          _appStateServiceProvider.isProfileCompleted;

      final bool isGoingToSplash = state.matchedLocation == splashPath;
      final bool isGoingToIntro = state.matchedLocation.startsWith(introPath);
      final bool isGoingToEmailVerify =
          state.matchedLocation.startsWith(emailVerifyPath);
      final bool isGoingToCompleteProfile =
          state.matchedLocation.startsWith(completeProfilePath);
      final bool isGoingToHome =
          state.matchedLocation.startsWith(bottomNavPrefix);

      if (!isInitialized && !isGoingToSplash) {
        return splashPath;
      }

      if (isInitialized && !isSignedIn && !isGoingToIntro) {
        return introPath;
      }
      if (isInitialized &&
          isSignedIn &&
          !isEmailVerified &&
          !isGoingToEmailVerify) {
        return emailVerifyPath;
      }
      if (isInitialized &&
          isSignedIn &&
          isEmailVerified &&
          !isProfileCompleted &&
          !isGoingToCompleteProfile) {
        return completeProfilePath;
      }
      if (isInitialized &&
          isSignedIn &&
          isEmailVerified &&
          isProfileCompleted &&
          !isGoingToHome) {
        return stateName.namedLocation(Pages.dashboard.toPathName());
      }
      return null;
    },
  );
}
