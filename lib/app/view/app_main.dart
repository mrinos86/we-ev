import 'dart:async';

import 'package:flutter/material.dart';
import 'package:we_ev/app/app.dart';
import 'package:we_ev/auth/auth.dart';
import 'package:we_ev/profile/profile.dart';
import 'package:we_ev/router/router.dart';
import 'package:we_ev/utils/utils.dart';

class WeEVApp extends StatefulWidget {
  const WeEVApp({super.key});

  @override
  State<WeEVApp> createState() => _WeEVAppState();
}

class _WeEVAppState extends State<WeEVApp> {
  late ThemeProvider _themeProvider;
  late AppRouter _appRouter;
  late AppStateServiceProvider _appStateServiceProvider;
  late AuthServiceProvider _authServiceProvider;
  late StreamSubscription<User?> _authSubscription;

  @override
  void initState() {
    _themeProvider = ThemeProvider();
    _appStateServiceProvider = AppStateServiceProvider.instance;
    _appRouter = AppRouter(_appStateServiceProvider);
    _authServiceProvider = AuthServiceProvider.instance;
    _authSubscription =
        _authServiceProvider.onAuthStateChanged.listen(onAuthStateChanged);
    super.initState();
  }

  void onAuthStateChanged(User? user) {
    _appStateServiceProvider.currentUser = user;
  }

  @override
  void dispose() {
    _authSubscription.cancel();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider<ThemeProvider>(create: (_) => _themeProvider),
        ChangeNotifierProvider<AppStateServiceProvider>(
            create: (_) => _appStateServiceProvider),
        ChangeNotifierProvider<AuthServiceProvider>(
            create: (_) => _authServiceProvider),
      ],
      builder: (context, child) => MaterialApp.router(
        debugShowCheckedModeBanner: false,
        title: 'We EV',
        themeMode: ThemeMode.light,
        theme: context.watch<ThemeProvider>().theme,
        routerConfig: _appRouter.goRouter,
        builder: (BuildContext context, Widget? child) =>
            AppLoader(child: child),
      ),
    );
  }
}
