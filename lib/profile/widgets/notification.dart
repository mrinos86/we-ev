import 'package:flutter/material.dart';
import 'package:we_ev/utils/utils.dart';

class Notifications extends StatefulWidget {
  final PageController pageController;
  const Notifications({
    Key? key,
    required this.pageController,
  }) : super(key: key);

  @override
  State<Notifications> createState() => _NotificationsState();
}

class _NotificationsState extends State<Notifications> {
  @override
  Widget build(BuildContext context) {
    return CustomScrollView(
      slivers: [
        SliverFillRemaining(
          hasScrollBody: false,
          fillOverscroll: true,
          child: Container(
            width: double.infinity,
            padding: const EdgeInsets.symmetric(horizontal: 3),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                sizedBoxHeight(25),
                Text(
                  "Notification",
                  style: size18_M_semiBold(textColor: primaryColor),
                ),
                sizedBoxHeight(15),
                Text(
                  "Let us notify you when your charging is \nclose to complete and provide\nyour direction as you drive.",
                  textAlign: TextAlign.center,
                  style: size16_M_regular(),
                ),
                sizedBoxHeight(75),
                const Image(
                  image: AssetImage(AppAssets.PROFILE_COMPLETE_EV_CHARGE),
                  height: 185,
                ),
                const Spacer(),
                ElevatedButton(
                  style: ElevatedButton.styleFrom(
                    minimumSize: Size(ScreenSize.getWidth(context, 0.6), 48),
                  ),
                  onPressed: onPressedContinueButton,
                  child: const Text("Continue"),
                ),
                sizedBoxHeight(10),
                OutlinedButton(
                  style: ElevatedButton.styleFrom(
                    minimumSize: Size(ScreenSize.getWidth(context, 0.6), 48),
                    side: const BorderSide(color: Colors.transparent),
                  ),
                  onPressed: onPressedContinueButton,
                  child: const Text('No, Thank you'),
                ),
                sizedBoxHeight(ScreenSize.getHeight(context, 0.04)),
              ],
            ),
          ),
        ),
      ],
    );
  }

  onPressedContinueButton() async {
    //   if (profileProvider.pickedImageFile != null) {
    //     AppStateServiceProvider.instance.showLoader;
    //     ApiResponse response = await ProfileRepository.instance
    //         .updateAvatar(File(profileProvider.pickedImageFile!));
    //     AppStateServiceProvider.instance.hideLoader;
    //     if (!response.result && mounted) {
    //       await showOKDialog(
    //         context: context,
    //         labelTitle: "Error !",
    //         labelContent: response.message,
    //       );
    //     } else if (response.result) {
    widget.pageController.nextPage(
      duration: const Duration(milliseconds: 300),
      curve: Curves.easeInOut,
    );
    //     }
    //   } else {
    //     await showOKDialog(
    //       context: context,
    //       labelTitle: "Error !",
    //       labelContent: "Please pick image to continue\n or Skip this process",
    //     );
    //   }
  }
}
