import 'package:flutter/material.dart';

class NotFoundPage extends StatelessWidget {
  final String error;
  const NotFoundPage({
    Key? key,
    required this.error,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Page Not Found"),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Text(error, style: const TextStyle(fontSize: 24)),
            ElevatedButton(
              onPressed: () {
                // Pages.home.go(context);
              },
              child: const Text("Go Back"),
            )
          ],
        ),
      ),
    );
  }
}
