import 'package:flutter/material.dart';
import 'package:we_ev/app/app.dart';
import 'package:we_ev/utils/utils.dart';

// ignore: must_be_immutable
class CustomAppBar extends StatelessWidget {
  String userName;
  String wishMsg;
  CustomAppBar(
      {Key? key, required this.userName, this.wishMsg = "Good Morning,"})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Align(
      alignment: Alignment.topCenter,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(wishMsg, style: size12_M_light(textColor: whiteColor)),
              Text(userName, style: size16_M_bold(textColor: whiteColor)),
            ],
          ),
          AppLogo(
            imageHeight: 45,
            imageWidth: 75,
          )
        ],
      ),
    );
  }
}
