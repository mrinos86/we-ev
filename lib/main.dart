import 'package:we_ev/app/view/app_main.dart';
import 'package:we_ev/bootstrap.dart';

void main() {
  bootstrap(() => const WeEVApp());
}
