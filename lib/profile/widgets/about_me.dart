import 'dart:io';

import 'package:flutter/material.dart';
import 'package:we_ev/common/common.dart';
import 'package:we_ev/common/provider/image_picker.dart';
import 'package:we_ev/utils/utils.dart';

class AboutMe extends StatefulWidget {
  final PageController pageController;
  const AboutMe({Key? key, required this.pageController}) : super(key: key);

  @override
  State<AboutMe> createState() => _AboutMeState();
}

class _AboutMeState extends State<AboutMe> {
  late TextEditingController _firstNameController;
  late TextEditingController _lastNameController;

  final GlobalKey<FormState> _formKey = GlobalKey();

  bool isFNameError = false;
  bool isLNameError = false;
  String errorMessage = "";

  String? pickedImageFile;

  @override
  void initState() {
    _firstNameController = TextEditingController();
    _lastNameController = TextEditingController();
    super.initState();
  }

  @override
  void dispose() {
    _firstNameController.dispose();
    _lastNameController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () => FocusScope.of(context).unfocus(),
      child: CustomScrollView(
        slivers: [
          SliverFillRemaining(
            hasScrollBody: false,
            fillOverscroll: true,
            child: Container(
              width: double.infinity,
              padding: const EdgeInsets.symmetric(horizontal: 3),
              child: Form(
                key: _formKey,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    sizedBoxHeight(20),
                    Text(
                      "About Me",
                      style: size18_M_semiBold(textColor: primaryColor),
                    ),
                    sizedBoxHeight(15),
                    Text(
                      "Upload your profile picture here",
                      style: size14_M_regular(),
                    ),
                    sizedBoxHeight(45),
                    Center(
                      child: GestureDetector(
                        onTap: () async {
                          await _uploadProfileImage(context);
                        },
                        child: SizedBox(
                          height: 120,
                          width: 120,
                          child: Stack(
                            children: [
                              Align(
                                alignment: Alignment.center,
                                child: CircleAvatar(
                                  radius: 120,
                                  child: ClipOval(
                                    child: pickedImageFile == null
                                        ? const Image(
                                            image: AssetImage(
                                              AppAssets.PROFILE_IMAGE,
                                            ),
                                            width: 120,
                                            height: 120,
                                            fit: BoxFit.fill,
                                          )
                                        : Image.file(
                                            File(pickedImageFile!),
                                            fit: BoxFit.fill,
                                            width: 120,
                                            height: 120,
                                          ),
                                  ),
                                ),
                              ),
                              const Align(
                                alignment: Alignment.bottomRight,
                                child: CircleAvatar(
                                    backgroundColor: primaryColor,
                                    radius: 18,
                                    child: ClipOval(
                                        child: Icon(
                                      Icons.add,
                                      size: 25,
                                      color: whiteColor,
                                    ))),
                              ),
                            ],
                          ),
                        ),
                      ),
                    ),
                    sizedBoxHeight(45),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          'First Name',
                          style: size14_M_regular(),
                        ),
                        sizedBoxHeight(10),
                        CustomTextField(
                          textController: _firstNameController,
                          isError: isFNameError,
                          keybordType: TextInputType.name,
                          textCapitalization: TextCapitalization.sentences,
                          onTapped: () {
                            setState(() {
                              isFNameError = false;
                            });
                          },
                          hintText: "",
                          labelText: "Enter first name here",
                        ),
                      ],
                    ),
                    sizedBoxHeight(10),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          'Last Name',
                          style: size14_M_regular(),
                        ),
                        sizedBoxHeight(4),
                        CustomTextField(
                          textController: _lastNameController,
                          isError: isLNameError,
                          keybordType: TextInputType.name,
                          textCapitalization: TextCapitalization.sentences,
                          onTapped: () {
                            setState(() {
                              isLNameError = false;
                            });
                          },
                          hintText: "",
                          labelText: "Enter last name here",
                        ),
                      ],
                    ),
                    sizedBoxHeight(25),
                    Align(
                      alignment: Alignment.centerLeft,
                      child: Text(
                        'Note: These information will not be shared',
                        style: size12_M_medium(),
                      ),
                    ),
                    // sizedBoxHeight(10),
                    const Spacer(),
                    Align(
                      alignment: Alignment.center,
                      child: ElevatedButton(
                        style: ElevatedButton.styleFrom(
                          minimumSize:
                              Size(ScreenSize.getWidth(context, 0.6), 48),
                        ),
                        onPressed: () async {
                          FocusManager.instance.primaryFocus?.unfocus();
                          onPressedContinueButton();
                        },
                        child: const Text("Continue"),
                      ),
                    ),
                    sizedBoxHeight(32),
                  ],
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }

  Future<void> _uploadProfileImage(BuildContext context) async {
    String? imagePath = await ImageUtil()
        .getImage(context: context, title: "Select Profile Picture");
    if (imagePath != null && imagePath.isNotEmpty) {
      setState(() {
        pickedImageFile = imagePath;
      });
    }
  }

  onPressedContinueButton() async {
    bool isError = validationCheck(
        fName: _firstNameController.text, lName: _lastNameController.text);
    if (isError) {
      await showOKDialog(
        context: context,
        labelTitle: "Error !",
        labelContent: errorMessage,
      );
    } else {
      widget.pageController.nextPage(
        duration: const Duration(milliseconds: 300),
        curve: Curves.easeInOut,
      );
    }
  }

  bool validationCheck({required String fName, required String lName}) {
    bool validationError = false;
    if (lName.isEmpty) {
      setState(() {
        isLNameError = true;
        errorMessage = "Please enter Last Name";
        validationError = true;
      });
    }
    if (fName.isEmpty) {
      setState(() {
        isFNameError = true;
        errorMessage = "Please enter First Name ";
        validationError = true;
      });
    }
    if (pickedImageFile == null) {
      setState(() {
        errorMessage = "Please pick Profile Image";
        validationError = true;
      });
    }

    return validationError;
  }
}
