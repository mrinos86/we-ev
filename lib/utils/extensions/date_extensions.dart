extension DateExtension on String {
  String? getMonthName() {
    String? value;
    switch (getMonthValue(this)) {
      case "01":
        value = "JAN";
        break;
      case "02":
        value = "FEB";
        break;
      case "03":
        value = "MAR";
        break;
      case "04":
        value = "APR";
        break;
      case "05":
        value = "MAY";
        break;
      case "06":
        value = "JUN";
        break;
      case "07":
        value = "JUL";
        break;
      case "08":
        value = "AUG";
        break;
      case "09":
        value = "SEP";
        break;
      case "10":
        value = "OCT";
        break;
      case "11":
        value = "NOV";
        break;
      case "12":
        value = "DEC";
        break;
    }
    return value;
  }

  String? getMonthFullName() {
    String? value;
    switch (getMonthValue(this)) {
      case "01":
        value = "JANUARY";
        break;
      case "02":
        value = "FEBRUARY";
        break;
      case "03":
        value = "MARCH";
        break;
      case "04":
        value = "APRIL";
        break;
      case "05":
        value = "MAY";
        break;
      case "06":
        value = "JUNE";
        break;
      case "07":
        value = "JULY";
        break;
      case "08":
        value = "AUGUST";
        break;
      case "09":
        value = "SEPTEMBER";
        break;
      case "10":
        value = "OCTOBER";
        break;
      case "11":
        value = "NOVEMBER";
        break;
      case "12":
        value = "DECEMBER";
        break;
    }
    return value;
  }
}

getMonthValue(String date) {
  var data = date.split("-");
  return data[1];
}

getDateValue(String date) {
  var data = date.split("-");
  return data[2];
}
