import 'package:dio/dio.dart';
import 'package:we_ev/common/common.dart';

ApiResponse<T?> onDioError<T>(DioError e) {
  if (e.response != null) {
    return ApiResponse<T?>(
      result: false,
      message: e.response?.data['message'] ?? '',
      payload: null,
    );
  } else {
    return ApiResponse<T?>(
      result: false,
      message: e.message ?? "",
      payload: null,
    );
  }
}

ApiResponse<T?> onError<T>(Object e) {
  return ApiResponse<T?>(
    result: false,
    message: e.toString(),
    payload: null,
  );
}
